<?php


class Person {
    public $firstName;
    public $middleName;
    public $lastName;

    public function __construct($firstName, $middleName, $lastName){
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }

    // Class methods

    public function printName(){
        return "Your full name is $this->firstName $this->middleName $this->lastName .";
    }
}

// [] Inheritance and Polymorphism

class Developer extends Person {
    public function printName(){
        return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer.";
}
}
class Engineer extends Person {
    public function printName(){
        return "Your are an engineer named  $this->firstName $this->middleName $this->lastName.";
}
}
// object creation from a class
$person = new Person('Jose', 'Webon', 'Yuan');
$developer = new Developer('John', 'Webon', 'Due');
$engineer = new Engineer('Jane', 'Shy', 'Oris');